/*
 * IP.c
 * 
 * Copyright 2020 Admin <Admin@DESKTOP-D5RV703>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */


#include <stdio.h>
#include <stdio.h>  // pour les entrées/sorties
#include <stdlib.h> // pour les allocations mémoire

#ifndef SIZE_MAX
#define SIZE_MAX (~(size_t)0)
#endif

#define ADRESS_SIZE 4


typedef unsigned char IP_Addr[ADRESS_SIZE];

typedef struct Node{
	size_t neighbour_nb;
	IP_Addr address;
	const struct Node** neighbours;
} Noeud;


Noeud* creation(unsigned char addr1, unsigned char addr2, unsigned char addr3, unsigned char addr4);
void sont_voisins(Noeud* node1, Noeud* node2);
size_t voisins_communs(Noeud* node1, Noeud* node2);
void affiche(Noeud* node);
void affiche_uno(const Noeud* node);
void node_destroy_neighbours(Noeud* node);


Noeud* creation(unsigned char addr1, unsigned char addr2, unsigned char addr3, unsigned char addr4){
	Noeud* result = calloc(1, sizeof(Noeud));
	if (result == NULL){
		fprintf(stderr, "Erreur (creation) : impossible d'allouer de la mémoire pour un nouveau Noeud (%u.%u.%u.%u).\n", addr1, addr2, addr3, addr4);
		return NULL;
	}
	else{
		result -> address[0] = addr1;
		result -> address[1] = addr2;
		result -> address[2] = addr3;
		result -> address[3] = addr4;
		result -> neighbour_nb = 0;
		result -> neighbours = NULL;
	}
	return result;
}

void node_destroy_neighbours(Noeud* node){
	if(node  != NULL){
		if(node -> neighbours != NULL){
			free(node -> neighbours);
		}
		free(node);
	}
}

void sont_voisins(Noeud* node1, Noeud* node2){
	if(node1 == NULL || node2 == NULL)
		return;
	++(node1 -> neighbour_nb);
	++(node2 -> neighbour_nb);
    Noeud const ** const old_content1 = node1->neighbours;
    Noeud const ** const old_content2 = node1->neighbours;
	if( node1 -> neighbour_nb > SIZE_MAX / sizeof(Noeud*) ||  node2 -> neighbour_nb > SIZE_MAX / sizeof(Noeud*) || 
		((node1 -> neighbours = realloc(node1-> neighbours, node1 -> neighbour_nb * sizeof(Noeud*))) == NULL) || ((node2 -> neighbours = realloc(node2-> neighbours, node2 -> neighbour_nb * sizeof(Noeud*))) == NULL)){
			node1->neighbours = old_content1;
			--(node1-> neighbour_nb);
			node2->neighbours = old_content2;
			--(node2-> neighbour_nb);
			fprintf(stderr, "Erreur (ajoute_voisin) : %u.%u.%u.%u a déjà trop de voisins.\n",
              node1->address[0] , node1->address[1], node1->address[2], node1->address[3]);
              return;
		}
	node1 -> neighbours[node1 -> neighbour_nb -1] = node2;
	node2 -> neighbours[node2 -> neighbour_nb -1] = node1;
	return;
}

size_t voisins_communs(Noeud* node1, Noeud* node2){
	size_t nb = 0;
	if (node1 == NULL || node2 == NULL)
		return nb;
	for (int i = 0; i < node1 -> neighbour_nb; i++)
	{
		for (int j = 0; j < node2 -> neighbour_nb; j++)
		{
			if(node1 -> neighbours[i] == node2 -> neighbours[j]);
				nb = nb+1;
		}
	}
	return nb;
}

void affiche_uno(const Noeud* node){
	if(node == NULL){
		printf("La node demandee est nulle");
		return;
	}
	printf("%u.%u.%u.%u", node -> address[0], node -> address[1], node -> address[2], node -> address[3]);
	return;
}
	
void affiche(Noeud* node){
	if(node == NULL){
		printf("La node demandee est nulle");
		return NULL;
	}
	printf("La node %u.%u.%u.%u a %Iu voisin(s) ", node -> address[0], node -> address[1], node -> address[2], node -> address[3], node -> neighbour_nb);
	for (int i = 0; i < node -> neighbour_nb; i++)
	{
		affiche_uno(node -> neighbours[i]);
	}
}

int main(int argc, char **argv)
{
	
	return 0;
}

