// This file requires at least C99 to compile

/**
 * @file   db.c
 * @author Merlin Nimier-David <merlin.nimier-david@epfl.ch>
 * @author Jean-Cédric Chappelier <jean-cedric.chappelier@epfl.ch>
 *
 * @copyright EPFL 2020
**/
/**
 * @section DESCRIPTION
 *
 * Template du homework du cours CS-207, année 2020.
**/

#include <stdio.h>
#include <stdlib.h> // EXIT_SUCCESS/FAILURE
#include <math.h>   // fabs()
#include <string.h> // memset()
#include <stdint.h> // uint32_t
#include <inttypes.h> // PRIu32 & SCNu32

// ----------------------------------------------
//   ___             _            _
//  / __|___ _ _  __| |_ __ _ _ _| |_ ___
// | (__/ _ \ ' \(_-<  _/ _` | ' \  _(_-<
//  \___\___/_||_/__/\__\__,_|_||_\__/__/

#define DB_MAX_SIZE    20u
#define QUERY_MAX_SIZE  5u
#define TEAM_DIFF_THRESHOLD 7
// ----------------------------------------------
//  _____
// |_   _|  _ _ __  ___ ___
//   | || || | '_ \/ -_|_-<
//   |_| \_, | .__/\___/__/
//       |__/|_|

/* Définissez ici les types demandés :
 *    StudentKind,
 *    SCIPER,
 *    Student,
 *    Database,
 * et QueryResult.
 */
 
typedef enum {Bachelor, Master, Exchange, StudentKindCount} StudentKind;
 
 typedef int SCIPER;
 
 typedef struct Student Student;
 struct Student{
	 SCIPER sciper;
	 double grade_sn;
	 double grade_hw;
	 double grade_exam;
	 StudentKind type;
	 const Student* teammate;
};
 /*
 typedef struct{
	 SCIPER sciper;
	 int grade_sn;
	 int grade_hw;
	 int grade_exam;
	 StudentKind categorie;
	 struct Student* teammate;
}Student;

*/
typedef Student Database [DB_MAX_SIZE];

typedef const Student* QueryResult[QUERY_MAX_SIZE]; //ATTENTION PAS SUR DU CONST !!!


// ----------------------------------------------
//   ___               _
//  / _ \ _  _ ___ _ _(_)___ ___
// | (_) | || / -_) '_| / -_|_-<
//  \__\_\\_,_\___|_| |_\___/__/

size_t db_entry_count(const Database db)
{
	size_t size;
	size = 0;
	while (db[size].sciper != 0 && size < DB_MAX_SIZE)
	{
		size += 1;
	}
    return size;
}

// ----------------------------------------------
const Student* get_student_by_sciper(const Database db, SCIPER sciper)
{
	for (int i = 0; i < DB_MAX_SIZE; i++)
	{
		if(db[i].sciper == sciper){
			const Student *student;
			student = &db[i];
			return student;
		}
	}
	return NULL;
}

// ----------------------------------------------
int check_teammates_consistency(const Database db)
{
	int consistency;
	consistency = 0;
	for (int i = 0; i < DB_MAX_SIZE; i++)
	{
		//initialise les deux potentiels teammates
		const Student student = db[i];
		const Student* teammate = student.teammate;
		//on check si le choix de teammate est valable 
		if(teammate != NULL){
			if(teammate -> teammate != &db[i]){
				//si il ne l'est pas on doit changer la valeur de retour à -1 et mentionner quel est le problème
				consistency = -1;
				//problème == il y a un champ null la  où il ne devrait pas en avoir
				if(teammate -> teammate == NULL)
					printf("%d a %d comme binome mais %d n'as pas de binome\n", student.sciper, teammate -> sciper, teammate -> sciper);
				//problème : pas le meme binome entre les deux teammate
				else
					printf("%d a %d comme binome mais %d a %d comme binome\n", student.sciper, teammate -> sciper, teammate -> sciper, teammate -> teammate -> sciper);
			}
		}
	}
    return consistency;
}

// ----------------------------------------------
void get_students_by_type(const Database db, StudentKind type, QueryResult result_out)
{
	int i,j;
	j = 0;
	for (i = 0; i < DB_MAX_SIZE; i++)
	{
		if (db[i].type == type && j < QUERY_MAX_SIZE && db[i].sciper != 0)
		{
			const Student* student;
			student = &db[i];
			result_out[j] = student;
			j++;
		}
	}
	if(j < QUERY_MAX_SIZE)
		result_out[j] = NULL;
}

// ----------------------------------------------
double grade_average(const Student* stud)
{
    if (stud == NULL) return 0.0;
    return 0.1 * stud->grade_hw + 0.4 * stud->grade_sn + 0.5 * stud->grade_exam;
}

// ----------------------------------------------
double team_diff(const Student* stud)
{
    return fabs(grade_average(stud) - grade_average(stud->teammate));
}

// ----------------------------------------------
#define student_to_index(tab, student) (size_t)((student) - (tab))

// ----------------------------------------------

int check_team_not_already_in_query (QueryResult result_out, const Student* student){ //renvoie 1 (True) si la team n est pas deja dans la query et 0 (False) si elle y est déjà (on évite ainsi les doublons)
	for (int i = 0; i < QUERY_MAX_SIZE; i++)
	{
		const Student* query_student = result_out[i];
		if(query_student == NULL)
			return 1;
		else if(query_student -> sciper == student -> sciper || query_student -> teammate -> sciper == student -> sciper)
			return 0;
	}
	return 1;
}


const Student* student_with_smallest_mean(const Student* student){ //renvoie un ptr vers le student avec la plus petite moyenne entre lui et son teammate
	if(grade_average(student -> teammate) < grade_average(student))
		return student -> teammate;
	return student;
}

int index_of_team_with_smallest_diff_in_average(QueryResult result_out){ //renvoie l'index de la team dans result_out avec la plus petite difference de moyenne
	int index = -1;
	double smallest_diff = TEAM_DIFF_THRESHOLD;
	for (int u = 0; u < QUERY_MAX_SIZE; u++)
	{
		if(result_out[u] == NULL)
			return index;
		else{
			if(team_diff(result_out[u]) < smallest_diff){
				smallest_diff = team_diff(result_out[u]);
				index = u;
			}
		}
	}
	return index;
}

void get_least_homegenous_teams(const Database db, QueryResult result_out)
{
	
	for (int z = 0; z < QUERY_MAX_SIZE; z++)
	{
		result_out[z] = NULL;
	}
	int i, j = 0;
	size_t size = db_entry_count(db);
	for (i = 0; i < size; i++) //fill the queryresult with the first QUERY_MAX_SIZE student
	{
		if((&db[i]) -> teammate != NULL){ //si le student a un teammate
			if(check_team_not_already_in_query(result_out, &db[i])){ // si ce teammate n'est pas déjà dans la query_result
				if(j >= QUERY_MAX_SIZE){ //on diferencie le cas où la query est déjà remplie et le cas où il reste de la place
					int smallest_diff_index = index_of_team_with_smallest_diff_in_average(result_out); //si plus de place on remplace si possible la plus petite diff
					if(team_diff(&db[i]) > team_diff(result_out[smallest_diff_index]))
						result_out[smallest_diff_index] = student_with_smallest_mean(&db[i]);
				}
				else																		//si il reste de la place on insère le student
					result_out[j] = student_with_smallest_mean(&db[i]);
				j++;
			}
		}
	}
}

// ----------------------------------------------
//  ___   _____
/*|_ _| / / _ \
//  | | / / (_) |
// |___/_/ \___/
*/
int load_database(Database db_out, const char* filename)
{
	FILE* entree;
	entree = fopen(filename, "r");
	
	SCIPER sciper, teammate_sciper;
	double grade_sn, grade_hw, grade_exam;
	int kind_nb;
	int i;
	i = 0;
	int return_value;
	return_value = 0;
	int check_sciper, check_grade_sn, check_grade_hw, check_grade_exam, check_kind,check_sciper_teammate;
	SCIPER teammate_sciper_values [DB_MAX_SIZE];
	StudentKind kind;
	
	if (entree == NULL)
	{
		fprintf(stderr, "Erreur: le fichier %s ne peut pas etre ouvert en ecriture !\n", filename);
		return_value = -1;
		return return_value;
	}
while (!feof(entree) && i < DB_MAX_SIZE)
	{
		int checker = 0;
		//on read et check si tout se passe bien pour le sciper
		check_sciper = fscanf(entree, "%" SCNu32, &sciper); // read SCIPER
		if(check_sciper == 1){															//check si on a pu lire le sciper
			//on read et check si tout se passe bien pour la grade sn
			check_grade_sn = fscanf(entree, "%lf", &grade_sn); // read grade_sn
			if(check_grade_sn == 1){												//check si on a pu lire grade_sn
				//on read et check si tout se passe bien pour le sciper	
				check_grade_hw = fscanf(entree, "%lf", &grade_hw); //read grade homework
				if(check_grade_hw == 1){												//check si on a pu read grade_homework
					//on read et check si tout se passe bien pour le sciper
					check_grade_exam = fscanf(entree, "%lf", &grade_exam); //read grade exam
					if(check_grade_exam == 1){												//check si on a pu read grade_exam
						//on read et check si tout se passe bien pour le sciper
						check_kind = fscanf(entree, "%d", &kind_nb); //read student kind
						check_kind = check_kind && (kind_nb < 3 && kind_nb > -1);
						if(check_kind == 1){ //check si on a pu lire le kind + si le kind est bien dans les bornes
							if(kind_nb == 0)
								kind = Bachelor;
							else if (kind_nb == 1)
								kind = Master;
							else if (kind_nb == 2)
								kind = Exchange;
							//read teammate sciper
							check_sciper_teammate = fscanf(entree, "%" SCNu32, &teammate_sciper); 
							if(check_sciper_teammate == 1){														//check si on a pu lire le sciper du teammate
								struct Student student = {.sciper = sciper, .grade_sn = grade_sn, .grade_hw = grade_hw, .grade_exam = grade_exam, .type = kind};
								db_out[i] = student;
								teammate_sciper_values[i] = teammate_sciper; //on met les scipers des teammate dans un tableau qu on consultera après pour assigner correctement les ptr vers ces teammates
							}	
							else{	
								fprintf(stderr, "le sciper du teammate de l'entree numero %d est invalide! \n", i+1);
								checker = 1;
							}
						}	
						else{
							fprintf(stderr, "le type de l'etudiant de l'entree numero %d est invalide! \n", i+1);
							checker = 1;
						}
					}
					else{
						fprintf(stderr, "la note de l'exam de l'entree numero %d est invalide! \n", i+1);
						checker = 1;
					}
				}
				else{
					fprintf(stderr, "la note hw de l'entree numero %d est invalide! \n", i+1);
					checker = 1;
				}
			}
			else{
				fprintf(stderr, "la note sn de l'entree numero %d est invalide! \n", i+1);
				checker = 1;
			}	
		}
		else{
			fprintf(stderr, "le sciper de l'entree numero %d est invalide! \n", i+1);
			checker = 1;
		}													
		
		//si il y a eu un probleme, on set le sciper de l etudiant a zero, on initialise aucun autre champ et on sort de la boucle (on arrete de lire le fichier) en mettant i = DB_MAX_SIZE
		if(checker) {  
				return_value = feof(entree) ? 0 : 1;
				sciper = 0;
				struct Student student = {.sciper = sciper};
				db_out[i] = student;
				i = DB_MAX_SIZE;
		}
		i++;
	}
	
	for (int j = 0; j < DB_MAX_SIZE; j++) // on itère sur la database et le tableau de sciper pour assigner à chaque student le teammate correspondant
	{
		Student* student = &db_out[j];
		if(student -> sciper == 0)
			break;
		if(teammate_sciper_values[j] == 0){
			student -> teammate = NULL;
		} 
		else
			student -> teammate = get_student_by_sciper(db_out, teammate_sciper_values[j] ); //assignation du ptr vers le teammate
	}
	
	
	fclose(entree);
	
	if (check_teammates_consistency(db_out) == -1) //on verifie que les binomes sont corrects 
		return_value = -1;
	return return_value;
}

// ----------------------------------------------
void fprintf_student_kind(FILE* restrict stream, StudentKind sk)
{
    switch (sk) {
    case Bachelor: fputs("bachelor", stream); break;
    case Master:   fputs("master  ", stream); break;
    case Exchange: fputs("exchange", stream); break;
    default:       fputs("unknown ", stream); break;
    }
}

// ----------------------------------------------
void write_student(const Student* student, FILE* fp)
{
    fprintf(fp, "%07d - %.2f, %.2f, %.2f - ",
            student->sciper, student->grade_sn, student->grade_hw,
            student->grade_exam);
    fprintf_student_kind(fp, student->type);
    if (student->teammate != NULL) {
        fprintf(fp, " - %06" PRIu32, student->teammate->sciper);
    } else {
        fprintf(fp, " - none");
    }
    fprintf(fp, "\n");
}

// ----------------------------------------------
int write_query_results(QueryResult result, const char* filename)
{
    FILE* fp = fopen(filename, "w");
    if (fp == NULL) {
        fprintf(stderr, "Error: could not open file %s for writting: ", filename);
        perror(NULL); // optionnel
        return -1;
    }
    for (size_t i = 0; i < QUERY_MAX_SIZE && result[i] != NULL; ++i) {
        write_student(result[i], fp);
    }
    fclose(fp);
    printf("Query results saved to: %s\n", filename);
    return 0;
}

// ----------------------------------------------
//  __  __      _
// |  \/  |__ _(_)_ _
/*// | |\/| / _` | | ' \
// |_|  |_\__,_|_|_||_|
*/
int main(int argc, char** argv)
{
    const char* input_filename = "many_teams.txt"; // default input filename
    if (argc >= 2) {
        input_filename = argv[1];
    }

    Database db;
    memset(db, 0, sizeof(db));
    int success = load_database(db, input_filename);
    if (success != 0) {
        fputs("Could not load database.\n", stderr);
        return EXIT_FAILURE;
    }

    // Print contents of database
    puts("+ ----------------- +\n"
         "| Students database |\n"
         "+ ----------------- +");
    const size_t end = db_entry_count(db);
    for (size_t i = 0; i < end; ++i) {
        write_student(&db[i], stdout);
    }

    // Extract students of each kind
    QueryResult res;
    memset(res, 0, sizeof(res));
    char filename[] = "res_type_00.txt";
    const size_t filename_mem_size = strlen(filename) + 1;
    for (StudentKind sk = Bachelor; sk < StudentKindCount; ++sk) {
        get_students_by_type(db, sk, res);
        snprintf(filename, filename_mem_size, "res_type_%02d.txt", sk);
        write_query_results(res, filename);
    }

    // Extract least homogeneous teams
    get_least_homegenous_teams(db, res);
    write_query_results(res, "bad_teams.txt");

    return EXIT_SUCCESS;
}
